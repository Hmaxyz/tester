import subprocess

def run(cmd, f=1):
	out = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).decode('utf-8')
	d = out.split("\n")
	filename =  d[0]
	if f>1:
		file2 = d[1]
		time = d[3].split(" ")
		print(time)
	else:
		time = d[1].split(" ")
		print(time)

	usert = time[0][0:4]
	systemt = time[1][0:4]
	realt = time[2][3:7]
	print("user\t{}\nsys\t{}\nreal\t{}\n".format(usert, systemt, realt))


def main():
	run('time -p python3 wc.py -l text.txt')
	run('time -p wc -l text.txt')

if __name__=="__main__":
	main()
