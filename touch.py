import subprocess
import os

def formatter(filename="", real=0, syst=0, user=0):
	cpu = float(user)+float(syst)
	cpuo = "{:.2f}".format(cpu)
	out = "{Testcase}\t{RealTime}\t{CPUTime}".format(Testcase=filename,RealTime=real,CPUTime=cpuo)
	lout = "{Testcase},{RealTime},{CPUTime}\n".format(Testcase=filename,RealTime=real,CPUTime=cpuo)
	with open("report_csv.csv", "a")  as log:
                log.writelines(lout)
	log.close()

	print(out)

def run(cmd, f=1):
	out = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).decode('utf-8')
	print(out)
	d = out.split("\n")
	l=0
	filename = ""
	real = 0
	user = 0
	syst = 0
	for x in d:
		if x is not "":
			y = x.split(" ")
			if l == 0:
				filename = y[-1]
			else:
				if y[0] == "real":
					real = y[1]
				elif y[0] == "user":
					user = y[1]
				elif y[0] == "sys":
					syst=y[1]
			l+=1
	formatter(filename=filename, real=real, syst=syst, user=user)

def main():
		
	testinputs = os.listdir("testinputs")
	print(testinputs)
	for testinput in testinputs:
		test = "time -p wc testinputs/" + testinput.strip()
		print(test)
		print("Testcase\tRealTime\tCPUTime")
		run(test)


if __name__=="__main__":
	main()
